package src.model;

import java.util.Optional;

import src.memento.MementoPanelChange;
import src.observerpattern.ExpiringMatchTimerObserver;
import src.observerpattern.TimerObserver;
import src.utilities.Utility;

/**
 * @author Jacopo Corina
 * The Class GameModel provide several methods that let you to manage MatchTimer and MatchScore
 */
public class GameStatusImpl implements GameStatus {
    private MementoPanelChange memento;
    private src.model.Timer timer;
    private int timerValue;
    private Score score;
    private Optional<ExpiringMatchTimerObserver> emtObserver;
    private Optional<TimerObserver> mtObserver;

    /**
     * Instantiates a new game model.
     *
     * @param timerValue the timer value
     * @param maxScore the max score
     */
    public GameStatusImpl(final int timerValue, final int maxScore) {
        this.memento = new MementoPanelChange();
        setTimerValue(timerValue);
        setMaximumScore(maxScore);
    }
    @Override
    public final void passObserverToExpiringTimer(final ExpiringMatchTimerObserver obs) {
        this.emtObserver = Optional.ofNullable(obs);
    }
    @Override
    public final void passObserverToTimer(final TimerObserver obs) {
        this.mtObserver = Optional.ofNullable(obs);
    }
    /**
     * Sets the timer value.
     *
     * @param timerValue the new timer value
     */

    public void setTimerValue(final int timerValue) {
        this.timerValue = timerValue;
    }

    /**
     * Sets the maximum score.
     *
     * @param max the new maximum score
     */
    public void setMaximumScore(final int max) {
        score = new ScoreImpl(max);
    }
    /**
     * Sets the player1 name.
     *
     * @param s the new player1 name
     */
    public void setPlayer1Name(final String s) {
        score.setPlayer1Name(s);
    }
    /**
     * Sets the player2 name.
     *
     * @param s the new player2 name
     */
    public void setPlayer2Name(final String s) {
        score.setPlayer2Name(s);
    }
    @Override
    public final void savePreviousPanelName(final String panelName) {
        memento.savePanelName(new String(panelName));
    }
    @Override
    public final String getPreviousPanelName() {
        return new String(memento.getPreviousPanelName());
    }
    /**
     * Adds the score player1.
     */
    public void addScorePlayer1() {
        this.score.addScorePlayer1();
    }
    /**
     * Adds the score player2.
     */
    public void addScorePlayer2() {
        this.score.addScorePlayer2();
    }
    /**
     * Gets the score1.
     *
     * @return the score1
     */
    public int getScore1() {
        return score.getScorePlayer1();
    }
    /**
     * Gets the score 2.
     *
     * @return the score 2
     */
    public int getScore2() {
        return score.getScorePlayer2();
    }
    /**
     * Reset score.
     */
    public void resetScore() {
        score.reset();
    }
    /**
     * Start match.
     */
    public void startMatch() {
        resetScore();
        if (mtObserver.isPresent() && emtObserver.isPresent()) {
            startMatchTimer(timerValue);
        }
    }
    /**
     * Start match timer.
     *
     * @param seconds the seconds
     */
    private void startMatchTimer(final int seconds) {
        timer = new MatchTimer(Utility.MSEC_CONST, Utility.MSEC_CONST * seconds, mtObserver.get(), emtObserver.get());
        timer.start();
    }
    /**
     * Pause match timer.
     */
    public void pauseMatchTimer() {
        if (timer != null) {
            timer.pause();
        }
    }
    /**
     * Resume match timer.
     */
    public void resumeMatchTimer() {
        if (timer != null) {
            timer.resume();
        }
    }
    /**
     * Reset match timer.
     */
    public void resetMatchTimer() {
        if (timer != null) {
            timer.cancel();
        }
    }
    /**
     * Gets the winner score mode.
     *
     * @return the winner score mode
     */
    public int getWinnerScoreMode() {
        return score.getWinnerWithMaximum();
    }
    /**
     * Gets the winner timer mode.
     *
     * @return the winner timer mode
     */
    public int getWinnerTimerMode() {
        return score.getWinner();
    }
    /**
     * Gets the score.
     *
     * @return the score
     */
    public Score getScore() {
        return new ScoreImpl((ScoreImpl) score);
    }

}

