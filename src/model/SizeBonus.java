package src.model;

import java.awt.Rectangle;

import src.observerpattern.ExpiringBonusTimerObserver;
import src.utilities.Direction;
/**
 * 
 * @author Federico Galdenzi
 * This class rappresents the Size bonus.
 * The player who take the bonus has the right to have the Giant ball when the ball pass into his lane
 * when the ball pass into the enemy's lane then it becomes a dwarf ball.
 */
public class SizeBonus extends BonusTimer {
    private Rectangle leftLane;
    private int bonusTo = -1;
    private static final int DWARF_DIAMETER = 15;
    private static final int GIANT_DIAMETER = 80;
    /**
     * This is the constructor of SizeBonus, we initialize the Timer class extended by BonusStrategy
     * @param interval the interval of the timer
     * @param duration the duration of the timer
     */
    /**
     * This is the constructor of the bonus timer which you have to pass the parameters.
     * @param ball the ball object
     * @param observer the ExpiringBonusTimerObserver object, in this case the BonusManager
     * @param interval the interval of the timer
     * @param duration the duration of the timer
     */
    public SizeBonus(final Ball ball, final ExpiringBonusTimerObserver observer, final int interval, final int duration) {
        super(ball, observer, interval, duration);
    }
    @Override
    public final void resetStrategy() {
        this.getBall().getProprierty().setDiameter(GameConstant.BALL_DIAMETER);
    }

    @Override
    public final void applyStrategy() {
        leftLane = new Rectangle(0, 0 - this.getBall().getProprierty().getDiameter(), this.getBall().getProprierty().getBoundaryWindow().getXBoundLaneRight() / 2, this.getBall().getProprierty().getBoundaryWindow().getYBoundLaneDown() + this.getBall().getProprierty().getDiameter());
        if (this.getBall().getProprierty().getXDirection() == Direction.RIGHT) {
            bonusTo = 1;
        } else {
            bonusTo = 2;
        }
        this.start();
    }

    private void setGiantBall() {
        this.getBall().getProprierty().setDiameter((int) (GIANT_DIAMETER * GameConstant.CONSTANT_OF_PROPORTION_X));
    }

    private void setDwarfBall() {
        this.getBall().getProprierty().setDiameter((int) (DWARF_DIAMETER * GameConstant.CONSTANT_OF_PROPORTION_X));
    }


    @Override
    public final void handleStrategy() {
        if (this.bonusTo == 1) {
            if (leftLane.contains(this.getBall().getShape())) {
                this.setGiantBall();
            } else {
                this.setDwarfBall();
            }
        } else {
            if (leftLane.contains(this.getBall().getShape())) {
                this.setDwarfBall();
            } else {
                this.setGiantBall();
            }
        }

    }

}
