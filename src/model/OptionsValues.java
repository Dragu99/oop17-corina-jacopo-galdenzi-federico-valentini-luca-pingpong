package src.model;

import java.awt.Color;
/**
 * This class contains informations about the game personalization.
 *
 * @author Jacopo Corina
 */
public class OptionsValues {
    private Color backgroundColor;
    private Color racket1Color;
    private Color racket2Color;
    private Color ballColor;
    private boolean soundEnabled;
    private int maxScore;
    private int timerSeconds;
    /**
     * Instantiates a new options values.
     */
    public OptionsValues() {
        backgroundColor = Color.BLACK;
        racket1Color = Color.WHITE;
        racket2Color = Color.WHITE;
        ballColor = Color.RED;
        soundEnabled = false;
        maxScore = GameConstant.VALUE_NOT_SET;
        timerSeconds = GameConstant.DEFAULT_MATCH_TIMER_VALUE;
    }
    /**
     * Gets the max score.
     *
     * @return the max score
     */
    public int getMaxScore() {
        return maxScore;
    }
    /**
     * Sets the max score.
     *
     * @param maxScore the new max score
     */
    public void setMaxScore(final int maxScore) {
        this.maxScore = maxScore;
    }
    /**
     * Gets the timer seconds.
     *
     * @return the timer seconds
     */
    public int getTimerSeconds() {
        return timerSeconds;
    }
    /**
     * Sets the timer seconds.
     *
     * @param timerSeconds the new timer seconds
     */
    public void setTimerSeconds(final int timerSeconds) {
        this.timerSeconds = timerSeconds;
    }
    /**
     * Checks if is sound enabled.
     *
     * @return true, if is sound enabled
     */
    public boolean isSoundEnabled() {
        return soundEnabled;
    }
    /**
     * Sets the sound enabled.
     *
     * @param soundEnabled the new sound enabled
     */
    public void setSoundEnabled(final boolean soundEnabled) {
        this.soundEnabled = soundEnabled;
    }
    /**
     * Gets the background color.
     *
     * @return the background color
     */
    public Color getBackgroundColor() {
        return backgroundColor;
    }
    /**
     * Sets the background color.
     *
     * @param backgroundColor the new background color
     */
    public void setBackgroundColor(final Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
    /**
     * Gets the racket 1 color.
     *
     * @return the racket 1 color
     */
    public Color getRacket1Color() {
        return racket1Color;
    }
    /**
     * Sets the racket 1 color.
     *
     * @param racket1Color the new racket 1 color
     */
    public void setRacket1Color(final Color racket1Color) {
        this.racket1Color = racket1Color;
    }
    /**
     * Gets the racket 2 color.
     *
     * @return the racket 2 color
     */
    public Color getRacket2Color() {
        return racket2Color;
    }
    /**
     * Sets the racket 2 color.
     *
     * @param racket2Color the new racket 2 color
     */
    public void setRacket2Color(final Color racket2Color) {
        this.racket2Color = racket2Color;
    }
    /**
     * Gets the ball color.
     *
     * @return the ball color
     */
    public Color getBallColor() {
        return ballColor;
    }
    /**
     * Sets the ball color.
     *
     * @param ballColor the new ball color
     */
    public void setBallColor(final Color ballColor) {
        this.ballColor = ballColor;
    }
}
