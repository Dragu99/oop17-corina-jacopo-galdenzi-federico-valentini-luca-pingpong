package src.model;

import java.awt.Point;
import java.awt.Rectangle;

import src.utilities.Direction;
/**
 * this is the racket class.
 * @author Luca Valentini
 *
 */
public class RacketImpl implements Racket {
    private int width; //properties of the paddle
    private int height;
    private Boundary boundary;
    private int xUp;
    private int yUp;
    /**
     * this is the constructor method of RacketImpl.
     * @param x position of the racket
     * @param y position of the racket
     * @param width of the racket
     * @param height of the racket
     * @param b represents the boundary of the field.
     */
    public RacketImpl(final int x, final int y, final int width, final int height, final Boundary b) {
        this.width = width;
        this.height = height;
        this.xUp = x;
        this.yUp = y;
        this.boundary = b;
    }
    @Override
    public final Point move(final Direction dir) {
        if (dir == Direction.DOWN) {        // up equivale al pulsante freccia in su
            if (this.yUp + this.height + (this.height / GameConstant.SLIDING_CONSTANT) >= this.boundary.getYBoundLaneDown()) {
                this.yUp = this.boundary.getYBoundLaneDown() - this.height;
            } else {
                this.yUp = this.yUp + this.height / GameConstant.SLIDING_CONSTANT;
            }
        } else if (dir == Direction.UP) {
            if (this.yUp - this.height / GameConstant.SLIDING_CONSTANT <= 0) {
                this.yUp = 0;
            } else {
                this.yUp = this.yUp - this.height / GameConstant.SLIDING_CONSTANT;
            }
        }
        return new Point(this.xUp, this.yUp);
    }
    @Override
    public final int getXUp() {
        return xUp;
    }
    @Override
    public final int getYUp() {
        return yUp;
    }
    @Override
    public final int getYDown() {
        return this.yUp + this.height;
    }
    @Override
    public final int getXDown() {
        return this.xUp + this.width;
    }
    @Override
    public final int getHeight() {
        return this.height;
    }
    @Override
    public final int getWidth() {
        return this.width;
    }
    @Override
    public final Rectangle getBoundRacket() {
        return new Rectangle(this.xUp, this.yUp, this.width, this.height);
    }
}
