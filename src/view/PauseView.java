package src.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JPanel;

import src.resources.ResourcesManagement;
import src.utilities.Utility;
import src.view.template.RoundedCornerButton;

/**
 * 
 * @author Jacopo Corina
 *
 */
public class PauseView extends JPanel {
    private static final long serialVersionUID = -3920618452114057000L;
    private JButton resumeButton = new RoundedCornerButton("RESUME");
    private JButton quitButton = new RoundedCornerButton("QUIT");

    /**
     * this is the constructor method of PauseView class.
     */
    public PauseView() {
        Optional<Font> appFont = Optional.ofNullable(Utility.fontLoader(ResourcesManagement.getCustomFontStream()));
        this.setBackground(Color.WHITE);
        this.setLayout(new GridLayout(2, 1, 0, 100));
        this.add(resumeButton);
        this.add(quitButton);
        appFont.ifPresent(x -> {
            resumeButton.setFont(x);
            quitButton.setFont(x);
        });
    }
    /**
     * 
     * @param l an action listener invoked on match resume
     */
    public final void addResumeButtonListener(final ActionListener l) {
        this.resumeButton.addActionListener(l);
    }
    /**
     * 
     * @param l an action listener invoked on match quit
     */
    public final void addQuitButtonListener(final ActionListener l) {
        this.quitButton.addActionListener(l);
    }
}
