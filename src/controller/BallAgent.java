package src.controller;

/**
 * 
 * @author Jacopo Corina
 *
 */

public interface BallAgent extends Runnable {
    /**
     * 
     * @return a boolean value indicating if the ball has stopped
     */
    boolean hasStopped();

}
