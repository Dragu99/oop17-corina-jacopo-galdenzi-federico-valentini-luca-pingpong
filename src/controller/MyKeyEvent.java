package src.controller;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
/**
 * 
 * @author Luca Valentini
 *
 */
public class MyKeyEvent extends KeyAdapter {
    private GameController ctrl;
    /**
     * 
     * @param ctrl is the game controller
     */
    public MyKeyEvent(final GameController ctrl) {
        this.ctrl = ctrl;
    }
    @Override
    public final synchronized void keyPressed(final KeyEvent e) {
        switch (e.getKeyCode()) {
        case KeyEvent.VK_S:
            this.ctrl.setDownPlayer1(true);
            break;
        case KeyEvent.VK_W:
            this.ctrl.setUpPlayer1(true);
            break;
        case KeyEvent.VK_UP:
            this.ctrl.setUpPlayer2(true);
            break;
        case KeyEvent.VK_DOWN:
            this.ctrl.setDownPlayer2(true);
            break;
        default:
            break;
        }
    }
    @Override
    public final synchronized void keyReleased(final KeyEvent e) {
        switch (e.getKeyCode()) {
        case KeyEvent.VK_S:
            this.ctrl.setDownPlayer1(false);
            break;
        case KeyEvent.VK_W:
            this.ctrl.setUpPlayer1(false);
            break;
        case KeyEvent.VK_UP:
            this.ctrl.setUpPlayer2(false);
            break;
        case KeyEvent.VK_DOWN:
            this.ctrl.setDownPlayer2(false);
            break;
        default:
            break;
        }
        this.ctrl.pauseGame(e.getKeyCode());
    }

    @Override
    public void keyTyped(final KeyEvent e) { }
}
