package src.observerpattern;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface ExpiringMatchTimerObserver {
    /**
     * executes actions when a match timer expires.
     */
    void executeOnMatchTimerExpiration();
}
